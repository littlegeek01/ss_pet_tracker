package com.ss.startup.pettracker.web;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ss.startup.pettracker.domain.OwnerService;
import com.ss.startup.pettracker.dto.CreateOwnerDto;
import com.ss.startup.pettracker.dto.PetLocationResponseDto;
import com.ss.startup.pettracker.dto.PetOwnerDto;

@RestController
@RequestMapping(path = "/owner")
public class OwnerController
{

    @Inject
    private OwnerService ownerService;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST)
    public UUID createOwner(@RequestBody CreateOwnerDto createOwnerDto)
    {
        return ownerService.createOwner(createOwnerDto);
    }

    @CrossOrigin
    @RequestMapping(path = "/pet", method = RequestMethod.POST)
    public void createOwnerPet(@RequestBody PetOwnerDto petOwnerDto)
    {
        ownerService.createOwnerPet(petOwnerDto);
    }

    @CrossOrigin
    @RequestMapping(path = "/{ownerUuid}/pet/{petUuid}/location", method = RequestMethod.GET)
    public List<PetLocationResponseDto> createOwnerPet(@PathVariable("ownerUuid") UUID ownerUuid,
            @PathVariable("petUuid") UUID petUuid)
    {
        return ownerService.getPetLocationForOwner(ownerUuid, petUuid);
    }
}
