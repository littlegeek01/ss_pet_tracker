package com.ss.startup.pettracker.web;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ss.startup.pettracker.domain.PetService;
import com.ss.startup.pettracker.dto.CreatePetDto;
import com.ss.startup.pettracker.dto.CreatePetLocationDto;
import com.ss.startup.pettracker.dto.PetLocationResponseDto;

@RestController
@RequestMapping(path = "/")
public class PetTrackerController
{

    @Inject
    private PetService petService;

    @CrossOrigin
    @RequestMapping(path = "/pet", method = RequestMethod.POST)
    public UUID createPet(@RequestBody CreatePetDto createPetDto)
    {
        return petService.createPet(createPetDto);
    }

    @CrossOrigin
    @RequestMapping(path = "/pet/{petUuid}/location", method = RequestMethod.POST)
    public Long createPetLocation(@PathVariable("petUuid") UUID petUuid,
            @RequestBody CreatePetLocationDto createPetLocationDto)
    {
        return petService.createPetLocation(petUuid, createPetLocationDto);
    }

    @CrossOrigin
    @RequestMapping(path = "/pet/{petUuid}/location", method = RequestMethod.GET)
    public List<PetLocationResponseDto> getPetLocations(@PathVariable("petUuid") UUID petUuid,
            @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime startDate,
            @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime endDate)
    {
        return petService.getPetLocations(petUuid, startDate, endDate);
    }
}
