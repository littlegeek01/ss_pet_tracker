package com.ss.startup.pettracker.dto;

import java.util.UUID;

public class PetOwnerDto
{
    private UUID ownerUuid;
    private UUID petUuid;

    public UUID getOwnerUuid()
    {
        return ownerUuid;
    }

    public void setOwnerUuid(UUID ownerUuid)
    {
        this.ownerUuid = ownerUuid;
    }

    public UUID getPetUuid()
    {
        return petUuid;
    }

    public void setPetUuid(UUID petUuid)
    {
        this.petUuid = petUuid;
    }

}
