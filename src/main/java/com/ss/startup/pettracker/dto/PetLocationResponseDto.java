package com.ss.startup.pettracker.dto;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PetLocationResponseDto
{
    private Long id;
    private Double latitude;
    private Double longitude;
    private OffsetDateTime createdTimestamp;
    private UUID petUuid;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(Double latitude)
    {
        this.latitude = latitude;
    }

    public Double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(Double longitude)
    {
        this.longitude = longitude;
    }

    public OffsetDateTime getCreatedTimestamp()
    {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(OffsetDateTime createdTimestamp)
    {
        this.createdTimestamp = createdTimestamp;
    }

    public UUID getPetUuid()
    {
        return petUuid;
    }

    public void setPetUuid(UUID petUuid)
    {
        this.petUuid = petUuid;
    }
}
