package com.ss.startup.pettracker.dao.entity;

import java.util.UUID;

public class PetEntity
{
    private UUID petUuid;
    private String name;

    public UUID getPetUuid()
    {
        return petUuid;
    }

    public void setPetUuid(UUID petUuid)
    {
        this.petUuid = petUuid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
