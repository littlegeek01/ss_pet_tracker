package com.ss.startup.pettracker.dao;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.ss.startup.pettracker.dao.entity.PetEntity;
import com.ss.startup.pettracker.dao.entity.PetLocationEntity;

@Named
@Singleton
public class PetDao
{
    private static final BeanPropertyRowMapper<PetLocationEntity> petLocationEntityMapper = BeanPropertyRowMapper.newInstance(PetLocationEntity.class);

    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void insertPet(PetEntity petEntity)
    {
        String sql = "INSERT INTO pet (pet_uuid, name) VALUES (:petUuid, :name)";
        jdbcTemplate.update(sql, new BeanPropertySqlParameterSource(petEntity));
    }

    public Long insertPetLocation(PetLocationEntity petLocationEntity)
    {
        String sql = "INSERT INTO pet_location(latitude, longitude, created_timestamp, pet_uuid) VALUES (:latitude, :longitude, :createdTimestamp, :petUuid) RETURNING id";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, new BeanPropertySqlParameterSource(petLocationEntity), keyHolder);
        return keyHolder.getKey().longValue();
    }

    public List<PetLocationEntity> findPetLocations(UUID petUuid, OffsetDateTime startDate, OffsetDateTime endDate)
    {
        String sql = "SELECT * FROM pet_location pl "
                + "JOIN pet p ON p.pet_uuid = pl.pet_uuid "
                + "WHERE p.pet_uuid = :petUuid "
                + "AND created_timestamp > :startDate "
                + "AND created_timestamp < :endDate";
        Map<String, Object> params = new HashMap<>();
        params.put("petUuid", petUuid);
        params.put("startDate", startDate);
        params.put("endDate", endDate);
        return jdbcTemplate.query(sql, params, petLocationEntityMapper);
    }
    
    
}
