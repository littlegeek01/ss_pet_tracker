package com.ss.startup.pettracker.dao;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ss.startup.pettracker.dao.entity.OwnerEntity;

@Named
@Singleton
public class OwnerDao
{
    private static final BeanPropertyRowMapper<OwnerEntity> ownerEntityRowMapper = BeanPropertyRowMapper.newInstance(OwnerEntity.class);
    
    @Inject
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public void insertOwner(OwnerEntity ownerEntity)
    {
        String sql = "INSERT INTO app_owner (owner_uuid, name, phone, address, subscription_type) VALUES (:ownerUuid, :name, :phone, :address, :subscriptionType)";
        namedParameterJdbcTemplate.update(sql, new BeanPropertySqlParameterSource(ownerEntity));
    }

    public void insertOwnerPet(UUID ownerUuid, UUID petUuid)
    {
        String sql = "INSERT INTO owner_pet (owner_uuid, pet_uuid) VALUES (:ownerUuid, :petUuid)";
        Map<String, Object> params = new HashMap<>();
        params.put("ownerUuid", ownerUuid);
        params.put("petUuid", petUuid);
        namedParameterJdbcTemplate.update(sql, params);
        
    }

    public OwnerEntity findOwner(UUID ownerUuid)
    {
        String sql = "SELECT * FROM app_owner WHERE owner_uuid = :ownerUuid";
        return namedParameterJdbcTemplate.queryForObject(sql, Collections.singletonMap("ownerUuid", ownerUuid), ownerEntityRowMapper);
    }
    
    
}
