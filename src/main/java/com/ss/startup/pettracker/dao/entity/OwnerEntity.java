package com.ss.startup.pettracker.dao.entity;

import java.util.UUID;

public class OwnerEntity
{
    private UUID ownerUuid;
    private String name;
    private String phone;
    private String address;
    private String subscriptionType;

    public UUID getOwnerUuid()
    {
        return ownerUuid;
    }

    public void setOwnerUuid(UUID ownerUuid)
    {
        this.ownerUuid = ownerUuid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getSubscriptionType()
    {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType)
    {
        this.subscriptionType = subscriptionType;
    }
}
