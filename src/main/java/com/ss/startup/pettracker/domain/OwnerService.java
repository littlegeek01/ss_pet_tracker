package com.ss.startup.pettracker.domain;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import com.ss.startup.pettracker.dao.OwnerDao;
import com.ss.startup.pettracker.dao.PetDao;
import com.ss.startup.pettracker.dao.entity.OwnerEntity;
import com.ss.startup.pettracker.dao.entity.PetLocationEntity;
import com.ss.startup.pettracker.dto.CreateOwnerDto;
import com.ss.startup.pettracker.dto.PetLocationResponseDto;
import com.ss.startup.pettracker.dto.PetOwnerDto;

@Named
@Singleton
public class OwnerService
{

    @Inject
    private OwnerDao ownerDao;
    
    @Inject
    private PetDao petDao;
    
    @Inject
    private ModelMapper modelMapper;
    
    
    public UUID createOwner(CreateOwnerDto createOwnerDto)
    {
        OwnerEntity ownerEntity = modelMapper.map(createOwnerDto, OwnerEntity.class);
        UUID ownerUuid = UUID.randomUUID();
        ownerEntity.setOwnerUuid(ownerUuid);
        ownerDao.insertOwner(ownerEntity);
        return ownerUuid;
    }


    public void createOwnerPet(PetOwnerDto petOwnerDto)
    {
        ownerDao.insertOwnerPet(petOwnerDto.getOwnerUuid(), petOwnerDto.getPetUuid());
    }


    public List<PetLocationResponseDto> getPetLocationForOwner(UUID ownerUuid, UUID petUuid)
    {
        OffsetDateTime endDate = OffsetDateTime.now();
        OffsetDateTime startDate = OffsetDateTime.now().minus(24, ChronoUnit.HOURS);
        OwnerEntity ownerEntity = ownerDao.findOwner(ownerUuid);
        if(ownerEntity.getSubscriptionType().equals("PREMIUM"))
        {
            startDate = OffsetDateTime.now().minus(30, ChronoUnit.DAYS);
        }
        
        List<PetLocationEntity> petLocationEntities = petDao.findPetLocations(petUuid, startDate, endDate);
        List<PetLocationResponseDto> petLocationResponseDtos = modelMapper.map(petLocationEntities, new TypeToken<List<PetLocationResponseDto>>() {}.getType());
        return petLocationResponseDtos;
    }
    
    
}
