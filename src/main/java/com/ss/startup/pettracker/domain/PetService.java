package com.ss.startup.pettracker.domain;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import com.ss.startup.pettracker.dao.PetDao;
import com.ss.startup.pettracker.dao.entity.PetEntity;
import com.ss.startup.pettracker.dao.entity.PetLocationEntity;
import com.ss.startup.pettracker.dto.CreatePetDto;
import com.ss.startup.pettracker.dto.CreatePetLocationDto;
import com.ss.startup.pettracker.dto.PetLocationResponseDto;

@Named
@Singleton
public class PetService
{

    @Inject
    private PetDao petDao;
    
    @Inject
    private ModelMapper modelMapper;

    public UUID createPet(CreatePetDto createPetDto)
    {
        PetEntity petEntity = modelMapper.map(createPetDto, PetEntity.class);
        UUID petUuid = UUID.randomUUID();
        petEntity.setPetUuid(petUuid);
        petDao.insertPet(petEntity);
        return petUuid;
    }

    public Long createPetLocation(UUID petUuid, CreatePetLocationDto createPetLocationDto)
    {
        PetLocationEntity petLocationEntity = modelMapper.map(createPetLocationDto, PetLocationEntity.class);
        petLocationEntity.setCreatedTimestamp(OffsetDateTime.now());
        petLocationEntity.setPetUuid(petUuid);
        return petDao.insertPetLocation(petLocationEntity);
    }

    public List<PetLocationResponseDto> getPetLocations(UUID petUuid, OffsetDateTime startDate, OffsetDateTime endDate)
    {
        List<PetLocationEntity> petLocationEntities = petDao.findPetLocations(petUuid, startDate, endDate);
        List<PetLocationResponseDto> petLocationResponseDtos = modelMapper.map(petLocationEntities, new TypeToken<List<PetLocationResponseDto>>() {}.getType());
        return petLocationResponseDtos;
    }
    
    
}
