CREATE DATABASE ss_pet_tracker OWNER lynn;

CREATE TABLE pet 
(
	pet_uuid UUID PRIMARY KEY,
	name TEXT NOT NULL
);

CREATE TABLE pet_location 
(
	id BIGSERIAL,
	latitude float8 NOT NULL,
	longitude float8 NOT NULL,
	created_timestamp TIMESTAMP NOT NULL,
	pet_uuid UUID REFERENCES pet(pet_uuid)
);

CREATE TABLE app_owner
(
	owner_uuid UUID PRIMARY KEY,
	name TEXT NOT NULL,
	phone TEXT,
	address TEXT,
	subscription_type TEXT
);

CREATE TABLE owner_pet
(
	id BIGSERIAL,
	owner_uuid UUID REFERENCES app_owner(owner_uuid),
	pet_uuid UUID REFERENCES pet(pet_uuid)
);

